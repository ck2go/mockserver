"""Test staticresponder."""
from genericserver.responders.staticresponder import StaticResponder


def test_response():
    request = 'test'
    default_response = 'myresoponse'
    responder = StaticResponder(default_response)
    response = responder.respondTo(request)
    assert response == default_response,\
        f"Response should always be 'myresponse'. Request: '{request}, Response: '{response}'"
