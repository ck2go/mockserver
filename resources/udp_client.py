import socket

host = 'localhost'
port = 20001

msg = None
while msg != 'q!':
    msg = input('Enter message: ')
    sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    bytes_sent = sock.sendto(str.encode(msg), (host, port))
    msgFromServer = sock.recvfrom(1024)
    print(f'Received: {msgFromServer[0].decode()}')
    sock.close()
