import socket

host = 'localhost'
port = 20001

msg = None
while msg != 'q!':
    msg = input('Enter message: ')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    sock.sendall(str.encode(msg))
    rec = sock.recv(1024)
    print(f'Received: {rec.decode()}')
    sock.close()
