"""Responder always answering with 'ok'."""

from .staticresponder import StaticResponder


class OkResponder(StaticResponder):
    """
    Responder that always returns 'ok'.
    """
    def __init__(self):
        super().__init__('ok')
