"""Responder always answering with 'ok'."""

from . import MockResponder


class StaticResponder(MockResponder):
    """
    Responder that always returns 'ok'.
    """
    def __init__(self, response):
        self.response = response


    def respondTo(self, request: str) -> str:
        """
        Get response to given request.

        Parameters
        ----------
        request:
            The string to be answered.

        Returns
        -------
            Always self.response (set in __init__).
        """
        return self.response
